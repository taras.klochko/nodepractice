import { runMePlease } from 'test-utils';
import utils from 'util';

const params = { password: 'sdf234fdsf32cdsc' };

const myFunct = utils.promisify(runMePlease);

try {
	const result = await myFunct(params);
	console.log(result);
} catch (error) {
	console.log(error);
}